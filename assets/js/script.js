const canvas = document.getElementById("the_canvas");
const context = canvas.getContext("2d");
const timer=canvas.getContext("2d");

//blue squares
context.fillStyle="#0000FF"
context.fillRect(1050, 450, 50, 50);

context.fillStyle="#0000FF"
context.fillRect(0, 0, 50, 50);

context.fillStyle="#0000FF"
context.fillRect(0, 450, 50, 50);

context.fillStyle="#0000FF"
context.fillRect(1050, 0, 50, 50);

//5 other complex shapes
//triangle
const triangle = canvas.getContext("2d");
triangle.beginPath();
triangle.moveTo(75, 40);
triangle.lineTo(200, 110);
triangle.lineTo(400,40);
triangle.lineTo(75,40);
triangle.fillStyle="#ff00ff"
triangle.fill();
triangle.stroke();


//circle
const circle = canvas.getContext("2d");
circle.beginPath();
circle.arc(150, 250, 80, 0, 2 * Math.PI);
circle.fillStyle="#00cc99"
circle.fill();
circle.stroke();

//pentagon
const pento = canvas.getContext("2d");
pento.beginPath();
pento.moveTo(400,200);
pento.lineTo(500,200);
pento.lineTo(520, 120);
pento.lineTo(450, 80);
pento.lineTo(380, 120);
pento.lineTo(400, 200);
pento.fillStyle="#F7BA6A";
pento.fill();
pento.stroke();

//parallelagram
const para = canvas.getContext("2d");
para.beginPath();
para.moveTo(400, 410);
para.lineTo(600, 410);
para.lineTo(540, 300);
para.lineTo(350, 300);
para.lineTo(400,410);
para.fillStyle="#5FBCDE"
para.fill();
para.stroke();

//hourglass
const hourglass = canvas.getContext("2d");
hourglass.beginPath();
hourglass.moveTo(600, 150);
hourglass.lineTo(650, 150);
hourglass.lineTo(600, 250);
hourglass.lineTo(650, 250);
hourglass.lineTo(600,150);
hourglass.fillStyle="#BEA6D1";
hourglass.fill();
hourglass.stroke();

//text

const text1 = canvas.getContext("2d");
text1.font = "30px Arial";
text1.fillText("Hello World", 500, 50);

const text2 = canvas.getContext("2d");
text2.font = "20px Times New Roman";
text2.fillText("Bye", 400, 460);

const text3 = canvas.getContext("2d");
text3.font = "100px Monaco";
text3.fillText("gamer", 670, 200);


let num=2;

if (num>1)
{
    let text="hello world";
    console.log(text);
}
else{
    console.log(text);
}

let timeEntered = new Date();
function secondsSinceEnter()
{
  return (new Date() - timeEntered) / 1000;
}



setInterval(function(){
    //context.fillRect(720,30,300,90);
    context.clearRect(720,30,300,90);
    timer.fillStyle="#69aa89";
    timer.fillText(Math.floor(secondsSinceEnter()),720,100);
    console.log(secondsSinceEnter());
}, 1000);
